Collection of practice problems for UTSC's CSCA08.

If you want to add something, push a merge request.
Don't forget, if you want, to put a license if you don't want it in public domain.

Unless specified, all code can be used as you like or dislike it.

Now to make sure you have read everything, put a delta on the top right corner.